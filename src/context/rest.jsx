import React from 'react';
import Constants from './constants.jsx';

class Rest {
    setupCallbacks(http, onSuccess, onError) {
        this.currentRequest = http;
        http.onerror = () => {
            if(this.onError != null) {
                onError(http.status);
            }
        }
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    try {
                        var response = http.response.split("\"taxonomy/").join("\"");
                        response = response.split("preferred-label").join("preferredLabel");
                        onSuccess(JSON.parse(response));
                    } catch(err) {
                        console.log("Exception", err);
                    }
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
    }

    abort() {
        if(this.currentRequest) {
            this.currentRequest.abort();
        }
        if(this.currentErrorCallback) {
            this.currentErrorCallback(499); //Client Closed Request
        }
        this.currentRequest = null;
    }

    get(ip, func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", ip + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    post(ip, func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", ip + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    postBody(ip, func, body, contentType, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", ip + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        if(contentType) {
            http.setRequestHeader("Content-Type", contentType);
        }
        http.send(body);
    }

    patch(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("PATCH", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    delete(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("DELETE", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    getPromise(ip, func) {
		return new Promise((resolve, reject) => {
			var http = new XMLHttpRequest();
			http.onerror = () => {
				reject(http.status);
			}
			http.onload = () => {
				if(http.status >= 200 && http.status < 300) {
					try {
						var response = http.response.split("\"taxonomy/").join("\"");
						response = response.split("preferred-label").join("preferredLabel");
						response = JSON.parse(response);
						resolve(response);
					} catch(err) {
						console.log("Exception", err);
					}
				} else {
					reject(http.status);
				}
			}
			http.open("GET", ip + func, true);
			http.setRequestHeader("api-key", Constants.REST_API_KEY);
			http.setRequestHeader("Accept", "application/json");
			http.send();
		});
    }

    getPostPromise(ip, func, json) {
        return new Promise((resolve, reject) => {
            var http = new XMLHttpRequest();
            http.onerror = () => {
                reject(http.status);
            }
            http.onload = () => {
                if(http.status >= 200 && http.status < 300) {
                    try {
                        var response = http.response.split("\"taxonomy/").join("\"");
                        response = response.split("preferred_label").join("preferredLabel");
                        response = JSON.parse(response);
                        resolve(response);
                    } catch(err) {
                        console.log("Exception", err);
                    }
                } else {
                    reject(http.status);
                }
            }
            http.open("POST", ip + func, true);
            http.setRequestHeader("Accept", "application/json");
            http.setRequestHeader("Content-Type", "application/json");
            http.send(JSON.stringify(json));
        });
    }

    getAutocompleteConcepts(query, version, type, onSuccess, onError) {
        this.get(Constants.REST_IP, "/suggesters/autocomplete?query-string=" + encodeURIComponent(query) + (version != null ? "&version=" + version : "") + (type != null ? "&type=" + type : ""), onSuccess, onError);
    }

    getSmartAutocompleteConcepts(query, type, limit, onSuccess, onError) {
        this.get(Constants.REST_ANNOTATION_API_IP, "/autocomplete/smart?q=" + encodeURIComponent(query) + (limit != null ? "&limit=" + limit : "") + (type != null ? "&types=" + type : ""), onSuccess, onError);
    }

    getSubstitutesFor(id) {
        var query = "query TaxonomyExample { "
         + "concepts (id: \"" + id + "\", type: \"occupation-name\") "
         + "{ substituted_by { id, type, preferred_label, id, substitutability_percentage } } "
         + "}";
        return this.getPromise(Constants.REST_IP, "/graphql?query=" + encodeURIComponent(query));
    }

    async awaitGetSubstitutesForPromise(id) {
        try {
            return await this.getSubstitutesFor(id);
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    getSkillsFor(id) {
        var query = "query TaxonomyExample { "
         + "concepts (id: \"" + id + "\") "
         + "{ broader (type: \"ssyk-level-4\") "
         + "{ id related(type: \"skill\") "
         + "{ id preferred_label broader(type: \"skill-headline\") {id} }"
         + "} } }";
        return this.getPromise(Constants.REST_IP, "/graphql?query=" + encodeURIComponent(query));
    }

    async awaitGetSkillsForPromise(id) {
        try {
            return await this.getSkillsFor(id);
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    getExactMatchSkillsFor(id) {
        var query = "query TaxonomyExample { "
         + "concepts (id: \"" + id + "\") "
         + "{ exact_match (type: \"skill\") "
         + "{ id preferred_label }"
         + "} }";
        return this.getPromise(Constants.REST_IP, "/graphql?query=" + encodeURIComponent(query));
    }

    async awaitGetExactMatchSkillsForPromise(id) {
        try {
            return await this.getExactMatchSkillsFor(id);
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    postSemanticSearch(json, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    try {
                        var response = http.response.split("\"taxonomy/").join("\"");
                        response = response.split("preferred_label").join("preferredLabel");
                        onSuccess(JSON.parse(response));
                    } catch(err) {
                        console.log("Exception", err);
                    }
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
        http.open("POST", Constants.REST_RECOMMENDER_API_IP+ "/semantic-concept-search/", true);
        http.setRequestHeader("Accept", "application/json");
        http.setRequestHeader("Content-Type", "application/json");
        http.send(JSON.stringify(json));
    }

    async awaitPostSemanticSearchPromise(json) {
        try {
            return await this.getPostPromise(Constants.REST_RECOMMENDER_API_IP, "/semantic-concept-search/", json);
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    postEducationDescription(text, onSuccess, onError) {
        this.abort();
        this.post(Constants.REST_ANNOTATION_API_IP, "/nlp/education-description?text=" + encodeURIComponent(text), onSuccess, onError);
    }
    
    postFileUpload(file, onSuccess, onError) {
        this.abort();
        var fd = new FormData();
        fd.append("file", file, file.name);
        this.postBody(Constants.REST_ANNOTATION_API_IP, "/files/upload-education-description", fd, null, onSuccess, onError);
    }
}

export default new Rest;