import React from 'react';

class Constants { 

    constructor() {
        // settings
        this.REST_IP = window.g_app_api_path;
        this.REST_API_KEY = window.g_app_api_key;
        this.REST_ANNOTATION_API_IP = window.g_app_annotation_api_path;
        this.REST_RECOMMENDER_API_IP = window.g_app_recommender_api_path;
        // events
        this.EVENT_SKILLS_CHANGED = "EVENT_SKILLS_CHANGED";
        this.EVENT_OCCUPATIONS_CHANGED = "EVENT_OCCUPATIONS_CHANGED";
        this.EVENT_EDUCATIONS_CHANGED = "EVENT_EDUCATIONS_CHANGED";
        this.EVENT_PAGE_SHOW_LOADING = "EVENT_PAGE_SHOW_LOADING";
        this.EVENT_PAGE_HIDE_LOADING = "EVENT_PAGE_HIDE_LOADING";
        this.EVENT_PAGE_SHOW_INDICATOR = "EVENT_PAGE_SHOW_INDICATOR";
        this.EVENT_PAGE_HIDE_INDICATOR = "EVENT_PAGE_HIDE_INDICATOR";
        this.EVENT_ACTIVE_PAGE = "EVENT_ACTIVE_PAGE";
    }
}

export default new Constants;