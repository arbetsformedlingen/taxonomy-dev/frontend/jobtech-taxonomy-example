import React from 'react';
import Rest from './rest.jsx';

class Context { 

    constructor() {
        this.occupations = [];
        this.skills = []; 
        this.educations = [];
    }

}

export default new Context;