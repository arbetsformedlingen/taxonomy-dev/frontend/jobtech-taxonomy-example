import React from 'react';
import Rest from './rest.jsx';

class Util { 

    constructor() {
        
    }

    async semanticSearch(words, type, limit) {
        var query = {
			array_of_words: words,
			concept_type: type,
			limit_number: limit
		}
		var data = await Rest.awaitPostSemanticSearchPromise(query);
        var res = [];
        if(data) {
            for(var i=0; i<words.length; ++i) {
                if(data[words[i]]) {
                    res.push(...data[words[i]]);
                }
            }
        }        
        return res;
    }

    filterDuplicates(arr) {
        var result = arr.reduce((unique, o) => {
            if(!unique.some(obj => obj.id === o.id)) {
              unique.push(o);
            }
            return unique;
        },[]);
        return result;
    }

    filterPresentInSecond(arr1, arr2) {
        var result = arr1.reduce((unique, o) => {
            if(!arr2.some(obj => obj.id === o.id)) {
                unique.push(o);
            }
            return unique;
        }, []);
        return result;
    }

    hasSameItems(arr1, arr2) {
        return this.filterPresentInSecond(arr1, arr2).length == 0;
    }

    sortByKey(items, key, direction) {
        items.sort((a, b) => {
            return this.sortValue(direction, a[key], b[key]);
        });
        return items;
    }

    sortValue(direction, aa, bb) {
        var a = aa;
        var b = bb;
        if(typeof(a) === "string" && typeof(b) === "string") {
            a = a.toLowerCase();
            b = b.toLowerCase();
            if(direction) {
                return a.localeCompare(b);
            } else {
                return -a.localeCompare(b);
            }
        } else if(direction) {
            if(a < b) return -1;
            if(a > b) return 1;
        } else {
            if(a < b) return 1;
            if(a > b) return -1;
        }
        return 0;
    }

    autoCompleteSmartSearch(string, version, type, onSuccess, onFail) {
		Rest.getSmartAutocompleteConcepts(string, type, 40, (data) => {
            onSuccess(data.concepts);
        }, (status) => {
            console.log("Failed to search", status);
            onFail(status);
        });
    }

    autoCompleteSemanticSearch(string, version, type, onSuccess, onFail) {
        var query = {
			array_of_words: [string],
			concept_type: type,
			limit_number: 40
		}
		Rest.postSemanticSearch(query, (data) => {
            var res = [];
            if(data) {
                if(data[string]) {
                    res.push(...data[string]);
                }
            }
            onSuccess(res);
        }, (status) => {
            console.log("Failed to search", status);
            onFail(status);
        });
	}
}

export default new Util;