import React from 'react';
import AutocompleteSearch from './../autocomplete_search.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Rest from './../context/rest.jsx';
import Util from './../context/util.jsx';
import Context from './../context/context.jsx';
import Constants from './../context/constants.jsx';

class EducationPage extends React.Component { 

	constructor() {
        super();
        this.state = {
            tags: [],
            suggestions: [],
        };
    }

    onSuggestedClicked(item) {
        Context.educations.push(item);
        this.state.suggestions.splice(this.state.suggestions.indexOf(item),1);
        EventDispatcher.fire(Constants.EVENT_EDUCATIONS_CHANGED);
        this.forceUpdate();
    }

    onTagClicked(item) {        
        Context.educations.splice(Context.educations.indexOf(item),1);
        EventDispatcher.fire(Constants.EVENT_EDUCATIONS_CHANGED);
        this.forceUpdate();
    }


    autocompleteSelected(item) {
        Context.educations.push({
            name: item.preferredLabel,
            id: item.id,
        });
        EventDispatcher.fire(Constants.EVENT_EDUCATIONS_CHANGED);
        this.forceUpdate();
    }

    renderCurrentTags() {
        var tags = Context.educations.map((element, index) => {
            return (
                <div 
                    className="tag"
                    onPointerUp={this.onTagClicked.bind(this, element)}
                    key={index}>
                    {element.name}
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Dina utbildningar</div>
                <div className="tag_container">
                    {tags}
                </div>
                <AutocompleteSearch 
                    placeholder="Sök efter utbildningar"
                    queryType="sun-education-field-4"
                    customSearch={Util.autoCompleteSmartSearch}
                    callback={this.autocompleteSelected.bind(this)}/>
            </div>
        );
    }

    renderSuggestedTags() {
        var tags = this.state.suggestions.map((element, index) => {
            return (
                <div 
                    className="tag"
                    onPointerUp={this.onSuggestedClicked.bind(this, element)}
                    key={index}>
                    {element.name}
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Du kanske också kan</div>
                <div className="tag_container">
                    {tags}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div 
                className="page"
                id="education_page">
                <div className="education">
                    <h1>Utbildningar</h1>
                    <div className="group">
                        {this.renderCurrentTags()}
                        {this.state.suggestions.length > 0 &&
                            this.renderSuggestedTags()
                        }
                    </div>
                </div>
            </div>
        );
    }
	
}

export default EducationPage;