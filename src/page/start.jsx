import React from 'react';
import Rest from './../context/rest.jsx';
import Context from './../context/context.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Util from './../context/util.jsx';

class StartPage extends React.Component { 

	constructor() {
        super();
       this.state = {
            result: null,
            fileUploaded: false,
        };
        this.boundGlobalDragEnter = this.onGlobalDragEnter.bind(this);
        this.boundGlobalDragOver = this.onGlobalDragOver.bind(this);
        this.boundGlobalDrop = this.onGlobalDrop.bind(this);
    }

    componentDidMount() {
        // TODO: might wanna rethink this since this is a scrolling page
        window.addEventListener("dragenter", this.boundGlobalDragEnter, false);
        window.addEventListener("dragover", this.boundGlobalDragOver);
        window.addEventListener("drop", this.boundGlobalDrop);
    }

    componentWillUnmount() {
        // TODO: might wanna rethink this since this is a scrolling page
        window.removeEventListener("dragenter", this.boundGlobalDragEnter);
        window.removeEventListener("dragover", this.boundGlobalDragOver);
        window.removeEventListener("drop", this.boundGlobalDrop);
    }

	onGlobalDragEnter(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDragOver(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDrop(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onDragEnter(e) {
        e.preventDefault();
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onDrop(e) {
        e.preventDefault();
        var file = e.dataTransfer.files[0];
        this.onFile(file);
		// reset component, wont be able to upload new file if we dont do this
		e.target.value = '';
    }

	onFile(file) {
        this.setState({fileUploaded: true});
		if(file.type == "text/plain") {
			var reader = new FileReader();
			reader.readAsText(file);
			reader.onload = (e) => {
				Rest.postEducationDescription(e.target.result, (data)=>{
                    this.populateFrom(data);
                }, (status)=>{
                    console.log("Error posting text: " + status);
                });
			};
		} else if(file.type == "application/pdf") {
			Rest.postFileUpload(file, (data)=>{
                    this.populateFrom(data);
                }, (status)=>{
                    console.log("Error posting file: " + status);
                });	
		} else {
            // unsupported format
        }
	}

    onFilePicker(e) {
        var file = e.target.files[0];      
        this.onFile(file);
		// reset component, wont be able to upload new file if we dont do this
		e.target.value = '';
    }

    onChooseFileClicked() {
        var filePicker = document.getElementById("file_picker");
        filePicker.click();
    }

    onNextClicked() {
        document.getElementById("education_page").scrollIntoView({
            behavior: "smooth", 
            block: "start", 
            inline: "start"
        });
    }

    onResetClicked() {
        this.setState({
            result: null,
            fileUploaded: false,
        });
        Context.educations = [];
        Context.occupations = [];
        Context.skills = [];
        EventDispatcher.fire(Constants.EVENT_EDUCATIONS_CHANGED);
        EventDispatcher.fire(Constants.EVENT_OCCUPATIONS_CHANGED);
        EventDispatcher.fire(Constants.EVENT_SKILLS_CHANGED);
    }

    async populateFrom(data) {        
        var populateType = (type, items) => {
            var list = [];
            for(var i=0; i<items.length; ++i) {
                var item = items[i];
                if(item.type == type) {
                    list.push({
                        name: item.preferredLabel,
                        id: item["concept-id"],
                    });
                }
            }
            return Util.filterDuplicates(list);
        }
        console.log(data);
        var educations = populateType("sun-education-field-4", data.annotations);
        var occupations = populateType("occupation-name", data.annotations);
        var skills = populateType("skill", data.annotations);
        skills.push(... await this.fetchExactSkills(populateType("esco-skill", data.annotations)));
        
        Context.educations = [];
        Context.educations.push(...educations);
        Context.occupations = [];
        Context.occupations.push(...occupations);
        Context.skills = [];
        Context.skills.push(...skills);
        this.setState({
            result: {
                educations: educations,
                occupations: occupations,
                skills: skills,
            },
        });
        EventDispatcher.fire(Constants.EVENT_EDUCATIONS_CHANGED);
        EventDispatcher.fire(Constants.EVENT_OCCUPATIONS_CHANGED);
        EventDispatcher.fire(Constants.EVENT_SKILLS_CHANGED);
    }

    async fetchExactSkills(escoSkills) {
        var res = [];
        if(escoSkills.length > 0) {
            for(var i=0; i<escoSkills.length; ++i) {
                var skills = await Rest.awaitGetExactMatchSkillsForPromise(escoSkills[i].id);
                if(skills.data.concepts[0].exact_match.length == 1) {
                    var skill = skills.data.concepts[0].exact_match[0];
                    res.push({
                        name: skill.preferred_label,
                        id: skill.id,
                    });
                }
            }
        }
        return res;
    }

    renderDragDropZone() {
        return (
            <div
                className="choose_import_drag_drop"
                id="drag_drop"
                accept=".pdf"
                onDragEnter={this.onDragEnter.bind(this)}
                onDragOver={this.onDragOver.bind(this)}
                onDrop={this.onDrop.bind(this)}>
                <div 
                    className="choose_import_dropzone"
                    id="dropzone">
                    Dra och släpp fil här (.txt, .pdf)
                </div> 
            </div>
        );
    }

    renderUploadFile() {
        return (
            <div className="start">
                <h1 className="small_margin">Ladda upp ditt CV</h1>
                <h5>Om du inte har ett CV, klicka på <b onPointerUp={this.onNextClicked.bind(this)}>
                    <i className="clickable">2.Utbildningar</i></b>
                </h5>
                <div className="group">
                    {this.renderDragDropZone()}
                    <div className="button_container">
                        <div 
                            className="button"
                            onPointerUp={this.onChooseFileClicked.bind(this)}>
                            Välj fil
                        </div>
                    </div>
                </div>
                <input 
                    id="file_picker" 
                    className="choose_import_button_file_picker" 
                    type="file"
                    accept=".pdf,.txt"
                    onChange={this.onFilePicker.bind(this)} />
            </div>
        );
    }

    renderResultInfo(list, type) {
        if(list.length > 0) {
            var items = list.map((item, i) => {
                return (<div key={i}>{item.name}</div>); 
            });
            return (
                <div className='start_result_info'>
                    <div>{list.length + " " + type}</div>
                    {items}
                </div>                
            );
        }
    }

    renderResult() {
        if(!this.state.result) {
            return(
                <div className="start">
                    <h1>CV laddar</h1>                
                    <div className="group">
                        <div className='indicator_loader'>
                        </div>
                    </div>
                    <input 
                        id="file_picker" 
                        className="choose_import_button_file_picker" 
                        type="file"
                        accept=".pdf,.txt"
                        onChange={this.onFilePicker.bind(this)} />
                </div>
            );
        }
        var info = "Vi har hämtat följande information från ditt CV";
        if(this.state.result.educations.length == 0 && this.state.result.occupations.length == 0 && this.state.result.skills.length == 0) {
            info = "Vi lyckades inte hitta några utbildningar kompetenser eller yrken i ditt cv";
        }

        return (
            <div className="start">
                <h1>CV uppladdat</h1>                
                <div className="group">
                    <div>
                        {info}
                        {this.renderResultInfo(this.state.result.educations, "utbildningar")}
                        {this.renderResultInfo(this.state.result.skills, "kompetenser")}
                        {this.renderResultInfo(this.state.result.occupations, "yrken")}
                    </div>
                    <div className="button_container">
                        <div 
                            className="button"
                            onPointerUp={this.onResetClicked.bind(this)}>
                            Börja om
                        </div>
                    </div>
                </div>
                <input 
                    id="file_picker" 
                    className="choose_import_button_file_picker" 
                    type="file"
                    accept=".pdf,.txt"
                    onChange={this.onFilePicker.bind(this)} />
            </div>
        );
    }

    render() {
        return (
            <div 
                className="page"
                id="start_page">
                {!this.state.fileUploaded &&
                    this.renderUploadFile()
                }
                {this.state.fileUploaded &&
                    this.renderResult()
                }
            </div>
        );
    }
	
}

export default StartPage;