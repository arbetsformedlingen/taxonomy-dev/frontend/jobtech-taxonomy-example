import React from 'react';
import Constants from './../context/constants.jsx';
import Rest from './../context/rest.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import AutocompleteSearch from './../autocomplete_search.jsx';
import Context from './../context/context.jsx';

class SummaryPage extends React.Component { 

	constructor() {
        super();
        this.state = {
            desiredOccupation: null,
            desiredOccupationSkills: []
        }
    }

    componentDidMount() {
        this.populateSkills();
    }

    async populateSkills() {
        var items = Context.occupations;
        for(var i=0; i<items.length; ++i) {
            if(!items[i].skills) {
                var data = await Rest.awaitGetSkillsForPromise(items[i].id);
                if(data && data.data.concepts[0]) {
                    items[i].skills = data.data.concepts[0].broader[0].related;
                } else {
                    console.error("no concept found with id: " + items[i].id);
                    items[i].skills = [];
                }
            }
        }
        this.forceUpdate();
    }

    async populateDesiredOccupation(occupation) {
        var data = await Rest.awaitGetSkillsForPromise(occupation.id);
        if(data.data.concepts.length == 0) {
            console.error("no concept found with id: " + occupation.id);
        }
        if(data) {
            this.setState({
                desiredOccupation: occupation,
                desiredOccupationSkills: data.data.concepts[0] ? data.data.concepts[0].broader[0].related : []
            });
        }
    }

    calculateSkillMatch(skills) {
        var matchCount = 0;
        if(skills) {
            for(var i=0; i<Context.skills.length; ++i) {
                var id = Context.skills[i].id;
                if(skills.find(e => e.id == id)) {
                    matchCount++;
                }
            } 
        }
        var percent = 0;
        if(matchCount > 0) {
            percent = matchCount / skills.length;
        }
        return Math.trunc(percent * 100);
    }

    getMatchColor(prc) {
        var css = "green";
        if(prc <= 20) {
            css = "red";
        } else if(prc <= 60) {
            css = "orange";
        }
        return css;
    }

    getSkillsWithParent(skills, id) {    
        if(skills) {    
            return skills.filter((e) => {
                if(e.broader && e.broader.length > 0) {
                    return e.broader[0].id == id;
                }
            });
        }
        return [];
    }

    autocompleteSelected(item) {
        this.populateDesiredOccupation(item);
    }

    getCertificates() {
        var result = [];
        for(var i=0; i<Context.occupations.length; ++i) {
            var skills = this.getSkillsWithParent(Context.occupations[i].skills, "s48H_Sft_DTn");
            for(var j=0; j<skills.length; ++j) {
                if(result.find(x => x.id == skills[j].id) == null) {
                    result.push(skills[j]);
                }
            }
        }
        return result;
    }

    renderCurrentTags() {
        var tags = Context.occupations.map((element, index) => {
            var prc = this.calculateSkillMatch(element.skills);
            var css = this.getMatchColor(prc);
            return (
                <div 
                    key={index}>
                    <div>{element.name}</div>
                    {element.skills ? <div className={css}><b>{prc}%</b></div> : <div className="indicator_loader smaller_loader"/>}
                </div>
            );
        });
        var certificates = this.getCertificates().map((element, index) => {
            var css = Context.skills.find(x => x.id == element.id) == null ? "red" : "green";
            return (
                <div 
                    key={index}
                    className={css}>
                    <b>{element.preferred_label}</b>
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Här listas din yrkeserfarenhet och hur väl de kompetenser du valt matchar dessa yrken</div>
                <div>En högre procentsats indikerar godare möjligheter för matchning</div>
                <div className="list_container summary_list">
                    {tags}
                </div>
                {certificates.length > 0 &&
                    <div className="sub_group">
                        <div>Till de yrken du har erfarenhet inom förekommer flera certifikat</div>
                        <div>Grön indikerar att du har dessa medans röda indikerar att du saknar dessa</div>
                        <div className="list_container summary_list">
                            {certificates}
                        </div>
                    </div>
                }
            </div>
        );
    }

    renderDesredOccupation() {
        var prc = this.calculateSkillMatch(this.state.desiredOccupationSkills);
        var certificates = [];
        var skills = this.getSkillsWithParent(this.state.desiredOccupationSkills, "s48H_Sft_DTn");
        for(var i=0; i<skills.length; ++i) {
            if(certificates.indexOf(skills[i]) == -1) {
                certificates.push(skills[i]);
            }
        }
        certificates = certificates.map((element, index) => {
            var css = Context.skills.find(x => x.id == element.id) == null ? "red" : "green";
            return (
                <div 
                    key={index}
                    className={css}>
                    <b>{element.preferred_label}</b>
                </div>
            );
        });
        return (  
            <div className="sub_group">
                <div className="list_container summary_list">
                    <div>
                        <div>{this.state.desiredOccupation.preferredLabel}</div>
                        <div className={this.getMatchColor(prc)}>
                            <b>{prc}%</b>
                        </div>
                    </div>
                </div>
                {certificates.length > 0 &&
                    <div className="sub_group">
                        <div>Till det nya yrket förekommer flera certifikat som kan vara nödvändiga eller meriterande</div>
                        <div>Grön indikerar att du har dessa medans röda indikerar att du saknar dessa</div>
                        <div className="list_container summary_list">
                            {certificates}
                        </div>
                    </div>
                }
            </div>
        );
    }

    renderNewOccupation() {
        return (
            <div className="sub_group">
                <div>Är du intresserad av ett helt annat yrke, sök här för att se hur väl dina nuvarande kompetenser och utbildningar matchar</div>
                <AutocompleteSearch 
                    placeholder="Sök efter yrken"
                    queryType="occupation-name"
                    callback={this.autocompleteSelected.bind(this)}/>
                {this.state.desiredOccupation &&
                    this.renderDesredOccupation()
                }
            </div>
        );
    }

    render() {
        return (
            <div 
                className="page1"
                id="summary_page">
                <div className="summary">
                    <div>
                        <h1 className="summary_group_title summar_top_title">Matchning</h1>
                        <div className="group">
                            {this.renderCurrentTags()}
                        </div>
                    </div>
                    <div>
                        <h1 className="summary_group_title">Nytt yrke</h1>
                        <div className="group">
                            {this.renderNewOccupation()}
                        </div>
                    </div>
                    <div>
                        <div className="group summary_bottom_margin">
                            Har du missat att lägga till något, gå tillbaka till föregående steg
                            <div 
                                className="button"
                                onPointerUp={EventDispatcher.fire.bind(EventDispatcher, Constants.EVENT_ACTIVE_PAGE, 0)}>
                                Tillbaka
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
	
}

export default SummaryPage;