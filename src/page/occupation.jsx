import React from 'react';
import AutocompleteSearch from './../autocomplete_search.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Util from './../context/util.jsx';
import Context from './../context/context.jsx';
import Constants from './../context/constants.jsx';
import Rest from '../context/rest.jsx';

class OccupationPage extends React.Component { 

	constructor() {
        super();
        this.state = {
            tags: [],
            suggestions: [],
        };
        this.boundSkillOrOccupationChanged = this.onSkillOrOccupationChanged.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundSkillOrOccupationChanged, Constants.EVENT_SKILLS_CHANGED);
        EventDispatcher.add(this.boundSkillOrOccupationChanged, Constants.EVENT_EDUCATIONS_CHANGED);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundSkillOrOccupationChanged, Constants.EVENT_SKILLS_CHANGED);
        EventDispatcher.remove(this.boundSkillOrOccupationChanged, Constants.EVENT_EDUCATIONS_CHANGED);
    }

    onSkillOrOccupationChanged() {
        this.fetchSuggestions();
    }

    onSuggestedClicked(item) {
        Context.occupations.push(item);
        this.state.suggestions.splice(this.state.suggestions.indexOf(item),1);
        EventDispatcher.fire(Constants.EVENT_OCCUPATIONS_CHANGED);
        this.fetchSuggestions(item.id);
        this.forceUpdate();
    }

    onTagClicked(item) {        
        Context.occupations.splice(Context.occupations.indexOf(item),1);
        EventDispatcher.fire(Constants.EVENT_OCCUPATIONS_CHANGED);
        this.forceUpdate();
    }

    autocompleteSelected(item) {
        Context.occupations.push({
            name: item.preferredLabel,
            id: item.id,
        });
        EventDispatcher.fire(Constants.EVENT_OCCUPATIONS_CHANGED);
        this.fetchSuggestions(item.id);
        this.forceUpdate();
    }

    async fetchSuggestions() {
        EventDispatcher.fire(Constants.EVENT_PAGE_SHOW_LOADING, "occupation_page");
        var begin = Date.now();
        var substitutes = [];
        for(var i=0; i<Context.occupations.length; ++i) {
            var data = await Rest.awaitGetSubstitutesForPromise(Context.occupations[i].id);
            if(data) {
                if(data.data.concepts.length > 0 && data.data.concepts[0].substituted_by.length > 0) {
                    var items = data.data.concepts[0].substituted_by;
                    for(var i=0; i<items.length; ++i) {
                        if(items[i].substitutability_percentage > 50) {
                            substitutes.push({
                                name: items[i].preferred_label,
                                id: items[i].id,
                            });
                        }
                    }
                }
            }
        }
        if(substitutes.length > 0) {            
            substitutes = Util.filterDuplicates(substitutes);
            substitutes = Util.filterPresentInSecond(substitutes, Context.occupations);
            if(substitutes.length > 15) {
                substitutes.length = 15;
            }
        }

        var tags = [];
        for(var i=0; i<Context.skills.length; ++i) {
            tags.push(Context.skills[i].name);
        }
        for(var i=0; i<Context.educations.length; ++i) {
            tags.push(Context.educations[i].name);
        }
        if(tags.length > 0) {            
            var data = await Util.semanticSearch(tags, "occupation-name", 5);
            if(data) {
                var suggestions = data.map((x) => {
                    return {
                        name: x.preferredLabel,
                        id: x.id,
                        distance: x.distance,
                    };
                });
                suggestions = Util.filterDuplicates(suggestions);
                suggestions = Util.filterPresentInSecond(suggestions, Context.occupations);
                suggestions = Util.sortByKey(suggestions, "distance", true);
                if(suggestions.length > 15) {
                    suggestions.length = 15;
                }
                suggestions.push(...substitutes);
                var isNew = suggestions.length != this.state.suggestions.length || !Util.hasSameItems(suggestions, this.state.suggestions);
                this.setState({suggestions: suggestions}, () => {
                    var timeout = 1500 - (Date.now() - begin);
                    setTimeout(() => {
                        EventDispatcher.fire(Constants.EVENT_PAGE_HIDE_LOADING, "occupation_page");
                        if(isNew) {
                            EventDispatcher.fire(Constants.EVENT_PAGE_SHOW_INDICATOR, "occupation_page");
                        }
                    }, timeout > 0 ? timeout : 0);
                });
            }
        } else {            
            this.setState({
                suggestions: []
            });
            EventDispatcher.fire(Constants.EVENT_PAGE_HIDE_LOADING, "occupation_page");
        }        
    }
    
    renderCurrentTags() {
        var tags = Context.occupations.map((element, index) => {
            return (
                <div 
                    className="tag"
                    onPointerUp={this.onTagClicked.bind(this, element)}
                    key={index}>
                    {element.name}
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Dina yrken</div>
                <div className="tag_container">
                    {tags}
                </div>
                <AutocompleteSearch 
                    placeholder="Sök efter yrken"
                    queryType="occupation-name"
                    customSearch={Util.autoCompleteSmartSearch}
                    callback={this.autocompleteSelected.bind(this)}/>
            </div>
        );
    }

    renderSuggestedTags() {
        var tags = this.state.suggestions.map((element, index) => {
            return (
                <div 
                    className="tag"
                    onPointerUp={this.onSuggestedClicked.bind(this, element)}
                    key={index}>
                    {element.name}
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Du kanske också kan</div>
                <div className="tag_container">
                    {tags}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div 
                className="page"
                id="occupation_page">
                <div className="occupation">
                    <h1>Yrken</h1>
                    <div className="group">
                        {this.renderCurrentTags()}
                        {this.state.suggestions.length > 0 &&
                            this.renderSuggestedTags()
                        }
                    </div>
                </div>
            </div>
        );
    }
	
}

export default OccupationPage;