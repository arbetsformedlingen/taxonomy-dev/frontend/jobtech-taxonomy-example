import React from 'react';
import Constants from '../context/constants.jsx';
import EventDispatcher from '../context/event_dispatcher.jsx';

class NextStepPage extends React.Component { 

	constructor() {
        super();
    }

    render() {
        return (
            <div 
                className="page"
                id="next_step_page">
                <div className="next_step">
                    <h1>Gå vidare</h1>
                    <div className="group">
                        Är du säker på att du fylt i alla dina kompetenser och din yrkeserfarenhet?<br/>
                        <div 
                            className="button"
                            onPointerUp={EventDispatcher.fire.bind(EventDispatcher, Constants.EVENT_ACTIVE_PAGE, 1)}>
                            Fortsätt
                        </div>
                    </div>
                </div>
            </div>
        );
    }
	
}

export default NextStepPage;