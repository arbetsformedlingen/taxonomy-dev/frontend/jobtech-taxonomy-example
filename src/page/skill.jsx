import React from 'react';
import AutocompleteSearch from './../autocomplete_search.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Util from './../context/util.jsx';
import Context from './../context/context.jsx';
import Constants from './../context/constants.jsx';

class SkillPage extends React.Component { 

	constructor() {
        super();
        this.state = {
            tags: [],
            suggestions: [],
        };
        this.boundOnEducationOrOccupationChanged = this.onEducationOrOccupationChanged.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundOnEducationOrOccupationChanged, Constants.EVENT_OCCUPATIONS_CHANGED);
        EventDispatcher.add(this.boundOnEducationOrOccupationChanged, Constants.EVENT_EDUCATIONS_CHANGED);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundOnEducationOrOccupationChanged, Constants.EVENT_OCCUPATIONS_CHANGED);
        EventDispatcher.remove(this.boundOnEducationOrOccupationChanged, Constants.EVENT_EDUCATIONS_CHANGED);
    }

    async onEducationOrOccupationChanged() {
        EventDispatcher.fire(Constants.EVENT_PAGE_SHOW_LOADING, "skill_page");
        var begin = Date.now();
        var tags = [];
        for(var i=0; i<Context.educations.length; ++i) {
            tags.push(Context.educations[i].name);
        }
        for(var i=0; i<Context.occupations.length; ++i) {
            tags.push(Context.occupations[i].name);
        }
        if(tags.length > 0) {
            var data = await Util.semanticSearch(tags, "skill", 5);
            if(data) {
                var suggestions = data.map((x) => {
                    return {
                        name: x.preferredLabel,
                        id: x.id,
                        distance: x.distance,
                    };
                });
                suggestions = Util.filterDuplicates(suggestions);
                suggestions = Util.filterPresentInSecond(suggestions, Context.skills);
                suggestions = Util.sortByKey(suggestions, "skill", true);
                if(suggestions.length > 30) {
                    suggestions.length = 30;
                }
                var isNew = suggestions.length != this.state.suggestions.length || !Util.hasSameItems(suggestions, this.state.suggestions);
                this.setState({suggestions: suggestions}, () => {
                    var timeout = 1500 - (Date.now() - begin);
                    setTimeout(() => {
                        EventDispatcher.fire(Constants.EVENT_PAGE_HIDE_LOADING, "skill_page");
                        if(isNew) {
                            EventDispatcher.fire(Constants.EVENT_PAGE_SHOW_INDICATOR, "skill_page");
                        }
                    }, timeout > 0 ? timeout : 0);
                });
            }
        } else {
            this.setState({
                suggestions: []
            });
            EventDispatcher.fire(Constants.EVENT_PAGE_HIDE_LOADING, "skill_page");
        }
    }

    onSuggestedClicked(item) {
        Context.skills.push(item);
        this.state.suggestions.splice(this.state.suggestions.indexOf(item),1);
        EventDispatcher.fire(Constants.EVENT_SKILLS_CHANGED);
        this.forceUpdate();
    }

    onTagClicked(item) {        
        Context.skills.splice(Context.skills.indexOf(item),1);
        EventDispatcher.fire(Constants.EVENT_SKILLS_CHANGED);
        this.forceUpdate();
    }

    autocompleteSelected(item) {
        Context.skills.push({
            name: item.preferredLabel,
            id: item.id,
        });
        EventDispatcher.fire(Constants.EVENT_SKILLS_CHANGED);
        this.forceUpdate();
    }

    renderCurrentTags() {
        var tags = Context.skills.map((element, index) => {
            return (
                <div 
                    className="tag"
                    onPointerUp={this.onTagClicked.bind(this, element)}
                    key={index}>
                    {element.name}
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Dina kompetenser</div>
                <div className="tag_container">
                    {tags}
                </div>
                <AutocompleteSearch 
                    placeholder="Sök efter kompetenser"
                    queryType="skill"
                    customSearch={Util.autoCompleteSmartSearch}
                    callback={this.autocompleteSelected.bind(this)}/>
            </div>
        );
    }

    renderSuggestedTags() {
        var tags = this.state.suggestions.map((element, index) => {
            return (
                <div 
                    className="tag"
                    onPointerUp={this.onSuggestedClicked.bind(this, element)}
                    key={index}>
                    {element.name}
                </div>
            );
        });
        return (
            <div className="sub_group">
                <div>Du kanske också kan</div>
                <div className="tag_container">
                    {tags}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div 
                className="page"
                id="skill_page">
                <div className="skill">
                    <h1>Kompetenser</h1>
                    <div className="group">
                        {this.renderCurrentTags()}
                        {this.state.suggestions.length > 0 &&
                            this.renderSuggestedTags()
                        }
                    </div>
                </div>
            </div>
        );
    }
	
}

export default SkillPage;