import React from 'react';
import Rest from './context/rest.jsx';

class AutocompleteSearch extends React.Component { 

	constructor() {
        super();
        this.state = {
            query: "",
            selected: null,
            autocomplete: [],
            isLoadingQuery: false,
            isLoadingContent: false,
        };
    }

    getIndicesOf(source, query) {
        var startIndex = 0;
        var index = 0;
        var indices = [];
        while((index = source.indexOf(query, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + query.length;
        }
        return indices;
    }

    getSuggestions() {
        if(this.props.customSuggestions) {
            return this.props.customSuggestions(this.state.autocomplete);
        }
        return this.state.autocomplete.filter((item) => {
            if(item.type == "occupation-collection" || 
               item.type == "unemployment-type") {
                return false;
            }
            return true;
        });
    }

    search(query) {
        this.query = query;
        if(query.length > 0) {
            Rest.abort();
            this.setState({
                query: query,
                autocomplete: [],
                isLoadingQuery: true,
            });
            this.performanceStart = performance.now();
            var func = this.props.customSearch ? this.props.customSearch : Rest.getAutocompleteConcepts.bind(Rest);
            func(query, "latest", this.props.queryType, (data) => {
                this.queryDuration = performance.now() - this.performanceStart;
                this.setState({
                    autocomplete: data,//Util.sortByKey(data, "preferredLabel", true),
                    isLoadingQuery: false,
                });
            }, () => {
                this.queryDuration = -1;
                this.setState({isLoadingQuery: false});
            });
        } else {
            Rest.abort();
            this.setState({
                query: query,
                autocomplete: [],
                isLoadingQuery: false,
            });
        }
    }

    onQueryChanged(e) {
        this.search(e.target.value);
    }

    onSuggestionClicked(item) {
        this.setState({
            isLoadingContent: true,
            query: this.props.keepQuery ? this.state.query : "",  
            autocomplete: [],
        });
        if(this.props.callback) {
            this.props.callback(item, {
                query: this.query,
                duration: this.queryDuration,
            });
        }
    }

    onGainedInputFocus() {
        if(this.state.query != null && this.state.query.length > 0) {
            this.search(this.state.query);
        }
    }

    onLostInputFocus() {
        if(this.state.isLoadingQuery) {
            Rest.abort();
        }
        setTimeout(() => {
            this.setState({
                autocomplete: [],
                isLoadingQuery: false,
            });
        }, 300);
    }

    renderItem(text) {
        var indices = this.getIndicesOf(text.toLowerCase(), this.state.query.toLowerCase());
        var index = 0;
        var result = [];
        for(var i=0; i<indices.length; ++i) {
            result.push({
                bold: false,
                text: text.substring(index, indices[i])
            });
            result.push({
                bold: this.props.highlightQuery != null ? this.props.highlightQuery : true,
                text: text.substring(indices[i], indices[i] + this.state.query.length)
            });
            index = indices[i] + this.state.query.length;
        }
        if(index < text.length) {
            result.push({
                bold: false,
                text: text.substring(index, text.length)
            });
        }
        result = result.filter((v) => {
            return v.text.length > 0;
        });
        return (
            <span>
                {result.map((item, i) => {
                    if(item.bold) {
                        return <b key={i}>{item.text}</b>;
                    } else {
                        return item.text;
                    }
                })}
            </span>
        );
    }

    renderSuggestions() {
        var items = this.getSuggestions();
        if(items.length) {
            return (
                <ul className="autocomplete">
                    {items.map((element, index) => {
                        if(this.props.onRenderItem) {
                            return (
                                <li 
                                    key={index}
                                    onMouseUp={this.onSuggestionClicked.bind(this, element)}>
                                    {this.props.onRenderItem(element)}
                                </li>
                            );
                        }
                        if(element.type == "occupation-collection") {
                            return (
                                <li 
                                    key={index}
                                    className="autocomplete_unavailable">
                                    {this.renderItem(element.preferredLabel)}
                                </li>
                            );
                        } else {
                            return (
                                <li 
                                    key={index}
                                    onMouseUp={this.onSuggestionClicked.bind(this, element)}>
                                    {this.renderItem(element.preferredLabel)}
                                </li>
                            );
                        }
                    })}
                </ul>
            );
        } else if(this.state.isLoadingQuery) {
            return (
                <div className="autocomplete">
                    <div className="indicator_loader smaller_loader"/>
                </div>
            );
        } else if(this.state.query.length > 0) {
            return (
                <div className="autocomplete">
                </div>
            );
        }
    }

    render() {
        return (
            <div className="autocomplete_input_field">
                <input 
                    type="text"
                    className="search_icon"
					placeholder={this.props.placeholder}
                    value={this.state.query}
                    onFocus={this.onGainedInputFocus.bind(this)}
                    onBlur={this.onLostInputFocus.bind(this)}
                    onChange={this.onQueryChanged.bind(this)}/>
                {this.renderSuggestions()}
            </div>
        );
    }
	
}

export default AutocompleteSearch;