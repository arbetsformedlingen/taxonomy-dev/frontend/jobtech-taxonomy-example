import React from 'react';
import ReactDOM from 'react-dom';
import Support from './support.jsx';
import StartPage from './page/start.jsx';
import EducationPage from './page/education.jsx';
import OccupationPage from './page/occupation.jsx';
import SkillPage from './page/skill.jsx';
import NextStepPage from './page/next_step.jsx';
import SummaryPage from './page/summary.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';

class Index extends React.Component { 

	constructor() {
        super();
		Support.init();
        this.state = {
            activeMenu: "start_page",
            page: 0,
        };
        this.observers = [];
        this.boundSetActivePage = this.onSetActivePage.bind(this);
        this.boundShowLoading = this.onPageShowLoading.bind(this);
        this.boundHideLoading = this.onPageHideLoading.bind(this);
        this.boundShowIndicator = this.onPageShowIndicator.bind(this);
        this.boundHideIndicator = this.onPageHideIndicator.bind(this);
    }

    setupObserver(id) {
        var observer = new IntersectionObserver((entries) => {
            if(entries[0].isIntersecting === true) {
                this.setState({activeMenu: id}, () => {
                    if(this.tid) {
                        clearTimeout(this.tid);
                        this.tid = null;
                    }
                    this.tid = setTimeout(() => {
                        this.onPageHideIndicator(id);
                        this.tid = null;
                    }, 1500);
                });
                if(id == "start_page") {
                    document.getElementById("up_arrow").classList.remove("indicator_visible");
                    document.getElementById("down_arrow").classList.add("indicator_visible");
                } else if(id == "next_step_page") {
                    document.getElementById("down_arrow").classList.remove("indicator_visible");
                    document.getElementById("up_arrow").classList.add("indicator_visible");
                } else {
                    document.getElementById("down_arrow").classList.add("indicator_visible");
                    document.getElementById("up_arrow").classList.add("indicator_visible");
                }
            }
        }, { 
            threshold: 0.5 
        });
        observer.observe(document.querySelector("#" + id));
        this.observers.push(observer);
    }

    setupObservers() {
        this.setupObserver("start_page");
        this.setupObserver("education_page");
        this.setupObserver("skill_page");
        this.setupObserver("occupation_page");
        this.setupObserver("next_step_page");
    }

    removeObservers() {
        for(var i=0; i<this.observers.length; ++i) {
            this.observers[i].disconnect();
        }
        this.observers = [];
    }

    componentDidMount() {
        EventDispatcher.add(this.boundSetActivePage, Constants.EVENT_ACTIVE_PAGE);
        EventDispatcher.add(this.boundShowLoading, Constants.EVENT_PAGE_SHOW_LOADING);
        EventDispatcher.add(this.boundHideLoading, Constants.EVENT_PAGE_HIDE_LOADING);
        EventDispatcher.add(this.boundShowIndicator, Constants.EVENT_PAGE_SHOW_INDICATOR);
        EventDispatcher.add(this.boundHideIndicator, Constants.EVENT_PAGE_HIDE_INDICATOR);
        this.setupObservers();
    }

    onSetActivePage(page) {
        this.removeObservers();
        this.setState({page: page}, () => {
            window.scrollTo(0, 0);
            if(page == 0) {
                this.setupObservers();
            }
        });
    }

    onPageShowLoading(page) {
        var element = document.getElementById(page + "_loader");
        if(element) {
            element.classList.add("indicator_visible");
        }
    }

    onPageHideLoading(page) {
        var element = document.getElementById(page + "_loader");
        if(element) {
            element.classList.remove("indicator_visible");
        }
    }

    onPageShowIndicator(page) {
        var element = document.getElementById(page + "_indicator");
        if(page != this.state.activeMenu && element) {
            element.classList.add("indicator_visible");
        }
    }

    onPageHideIndicator(page) {
        var element = document.getElementById(page + "_indicator");
        if(element) {
            element.classList.remove("indicator_visible");
        }
    }

    onMenuItemClicked(id) {
        document.getElementById(id).scrollIntoView({
            behavior: "smooth", 
            block: "start", 
            inline: "start"
        });
    }

    onNextClicked() {
        switch(this.state.activeMenu) {
            case "start_page":
                this.onMenuItemClicked("education_page");
                break;
            case "education_page":
                this.onMenuItemClicked("skill_page");
                break;
            case "skill_page":
                this.onMenuItemClicked("occupation_page");
                break;
            case "occupation_page":
                this.onMenuItemClicked("next_step_page");
                break;
        }
    }

    onPreviousClicked() {
        switch(this.state.activeMenu) {
            case "education_page":
                this.onMenuItemClicked("start_page");
                break;
            case "skill_page":
                this.onMenuItemClicked("education_page");
                break;
            case "occupation_page":
                this.onMenuItemClicked("skill_page");
                break;
            case "summary_page":
                this.onMenuItemClicked("occupation_page");
                break;
        }
    }

    renderIndicator(id) {
        return (
            <div
                id={id + "_indicator"} 
                className="indicator_changes"/>
        );
    }

    renderLoader(id) {
        return (
            <div
                id={id + "_loader"} 
                className="indicator_loader_group">
                <div className="indicator_loader"/>
            </div>
        );
    }

    renderMenuItem(text, id) {
        return (
            <div 
                className={this.state.activeMenu == id ? "active" : ""}
                onPointerUp={this.onMenuItemClicked.bind(this, id)}>
                <div className="menu_item">
                    {text}
                    {this.renderIndicator(id)}
                </div>
                {this.renderLoader(id)}
            </div>
        );
    }

    renderArrows() {
        return (
            <div className="arrow_container">
                <div 
                    id="up_arrow"
                    className="arrow_container_top arrow up clickable"
                    onPointerUp={this.onPreviousClicked.bind(this)}/>
                <div 
                    id="down_arrow"
                    className="arrow_container_bottom arrow down clickable"
                    onPointerUp={this.onNextClicked.bind(this)}/>
            </div>
        );
    }

    page0() {
        return (
            <div className="page_container">
                <div className="side_container">
                    <div className="logo">
                        <img src="./resources/JobTechDevelopment_black.png"/>
                    </div>
                    <div className="side_menu">
                        {this.renderMenuItem("1. Start", "start_page")}
                        {this.renderMenuItem("2. Utbildningar", "education_page")}
                        {this.renderMenuItem("3. Kompetenser", "skill_page")}
                        {this.renderMenuItem("4. Yrken", "occupation_page")}
                        {this.renderMenuItem("5. Gå vidare", "next_step_page")}
                    </div>
                </div>
                <div className="content_container">
                    <StartPage/>
                    <EducationPage/>
                    <SkillPage/>
                    <OccupationPage/>  
                    <NextStepPage/>  
                </div>
                {this.renderArrows()}
            </div>
        );
    }

    page1() {
        return (
            <div className="page_container">
                <div className="side_container">
                    <div className="logo">
                        <img src="./resources/JobTechDevelopment_black.png"/>
                    </div>
                </div>
                <div className="content_container">
                    <SummaryPage/>
                </div>
            </div>
        );
    }

    render() {
        return this.state.page == 0 ? this.page0() : this.page1();
    }
	
}

ReactDOM.render(<Index/>, document.getElementById('content'));