FROM node:18.17.1-slim

COPY package*  webpack* /app/
COPY src/  /app/src/
COPY config.js  /app/config.js
COPY build_finalizer.js  /app/build_finalizer.js
#COPY . /app/
WORKDIR /app

# this part just makes sure there arent any compile errors
RUN npm install
RUN npm run release

EXPOSE 8000
CMD npm run  build
