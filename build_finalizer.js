var fs = require('fs');

var errorLog = function(v) {
	if(v) {
		console.log(v);
	}
};

var deleteFolderRecursive = function(path) {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach(function(file,index){
			var curPath = path + "/" + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse
				deleteFolderRecursive(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
};

var copyFolderRecursive = function(path, destination) {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach(function(file,index){
			var curPath = path + "/" + file;
			var dstPath = destination + "/" + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse			
				fs.mkdirSync(dstPath, errorLog);
				copyFolderRecursive(curPath, dstPath);
			} else { // copy file
				fs.copyFile(curPath, dstPath, errorLog);
			}
		});
	}
};

var replaceHtmlContent = function(htmlFile, files) {
	fs.readFile(htmlFile, 'utf8', function (err, data) {
		if (err) {
			return console.log(err);
		}
		var result = data;		
		for(var i=0; i<files.length; ++i) {
			if(files[i].replace) {
				result = result.replace(files[i].src, files[i].dst);
			}
		}
		fs.writeFile(htmlFile, result, 'utf8', errorLog);
	});
};

try {
	var startTime = Date.now();
	console.log('\nSetting up release package...');
	
	var time = Date.now();
	var srcRoot = './src/';
	var dstRoot = './target/';
	var htmlFileDsts = [];
	htmlFileDsts.push(dstRoot + 'index.html');
	var files = [
		{ src: 'index.html', dst: 'index.html' },
		{ src: 'css/app.css', dst: 'style/app_' + time + '_.css', replace: true },
		{ src: 'js/index.js', dst: 'js/index_' + time + '_.js', replace: true },
		{ src: 'js/index.js.LICENSE.txt', dst: 'js/index_' + time + '_.js.LICENSE.txt', replace: true },
	];
	
	// remove release package to ensure unwanted files are removed
	console.log('Removing old target folder...');
	deleteFolderRecursive('./target');
	
	// setup release package
	console.log('Setting up new target...');
	fs.mkdirSync('./target', errorLog);
	fs.mkdirSync('./target/js', errorLog);
	fs.mkdirSync('./target/page', errorLog);
	fs.mkdirSync('./target/resources', errorLog);
	fs.mkdirSync('./target/style', errorLog);
	
	// copy files
	console.log('Copying files...');
	for(var i=0; i<files.length; ++i) {
		fs.copyFile(srcRoot + files[i].src, dstRoot + files[i].dst, errorLog);
	}
	copyFolderRecursive('./src/resources', './target/resources');
	
	// append name replacements
	files.push( {src: './../config.js', dst: './config.js', replace: true} );
	files.push( {src: './../../config.js', dst: './../config.js', replace: true} );
	
	// rename files in html file
	console.log('Finalizing files...');
	for(var i=0; i<htmlFileDsts.length; ++i) {
		replaceHtmlContent(htmlFileDsts[i], files);
	}
	
	console.log('-----------------------');
	console.log('Build complete!');
	console.log('Finalizing took: ' + (Date.now() - startTime) + " ms");
} catch(e) {
	console.log(e);
}
